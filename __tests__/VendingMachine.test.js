import VendingMachine from '../src/VendingMachine';

describe('Vending Machine', () => {
  test('it should exist', () => {
    const vending = new VendingMachine();
    expect(vending).toBeDefined();
  });
});
